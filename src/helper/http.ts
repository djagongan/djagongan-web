import axios, { AxiosError } from "axios";
import * as Logger from 'axios-logger';
import { StorageKey } from ".";

const { VITE_API_URL } = import.meta.env

const http = axios.create({
  baseURL: VITE_API_URL
})

http.interceptors.request.use((config) => {
  const token = sessionStorage.getItem(StorageKey.ACCESS_TOKEN)
  if (token) {
    config.headers.Authorization = 'Bearer ' + token
  }

  Logger.requestLogger(config)

  return config
}, Logger.errorLogger)

http.interceptors.response.use(
  (response) => {
    return Logger.responseLogger(response)
  },
  (error: AxiosError) => {
    if (error.response?.status === 401) {
      sessionStorage.removeItem(StorageKey.ACCESS_TOKEN)
      sessionStorage.removeItem(StorageKey.REFRESH_TOKEN)
      location.replace('/signin')
    }

    return Logger.errorLogger(error)
  }
)

export {
  http
};
