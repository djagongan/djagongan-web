export interface IPayloadLogin {
  username: string
  password: string
}

export interface IResponseLogin {
  accessToken: string
  refreshToken: string
}