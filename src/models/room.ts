import { IAudit } from "@models";

export type TRoomType = 'direct' | 'group';
export type TRoomUserRole = 'owner' | 'admin' | 'user';

export interface IRoomUser {
  _id: string;
  role: TRoomUserRole;
  joinedAt: string;
  leaveAt?: string;
}

export interface IRoom extends IAudit {
  _id: string;
  type: TRoomType;
  name: string;
  image: string;
  users: IRoomUser[];
  isPinned: boolean;
  isMuted: boolean;
}
