import { IAudit, IPayloadLogin } from "@models";

export interface IUser extends IPayloadLogin, IAudit {
  _id: string;
  firstName: string;
  lastName: string;
  image: string;
  email: string;
  isActive: boolean;
}
