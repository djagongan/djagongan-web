export interface IAudit {
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}