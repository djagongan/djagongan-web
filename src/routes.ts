import { RouteDefinition } from '@solidjs/router';
import { lazy } from 'solid-js';

const routes: RouteDefinition[] = [
  {
    path: '/signin',
    component: lazy(() => import('./components/guards/Authenticated')),
    children: [{
      path: '/',
      component: lazy(() => import('./pages/LoginPage'))
    }]
  },
  {
    path: '/',
    component: lazy(() => import('./components/guards/Unauthenticated')),
    children: [
      {
        path: '/',
        component: lazy(() => import('./pages/HomePage'))
      },
      {
        path: '/about',
        component: lazy(() => import('./pages/AboutPage'))
      },
      {
        path: '/profile',
        component: lazy(() => import('./pages/ProfilePage'))
      }
    ]
  },
];

export default routes;
