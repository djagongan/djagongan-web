import { IPayloadLogin } from '@models';
import { authService } from '@services';
import { useNavigate } from '@solidjs/router';
import { useStore } from '@store';
import { Component, createSignal } from 'solid-js';
import TextField from '../inputs/TextField';

const Login: Component<{}> = () => {
  const navigate = useNavigate();
  const [, { setProfile }] = useStore();

  const [credentials, setCredentials] = createSignal<IPayloadLogin>({
    username: '',
    password: ''
  });

  const onSubmit = async () => {
    const ok = await authService.login(credentials());
    if (ok) {
      navigate('/', { replace: true });
    }
  };

  return (
    <div class="relative flex min-h-screen text-gray-800 antialiased flex-col justify-center overflow-hidden bg-gray-50 py-6 sm:py-12">
      <div class="relative py-3 sm:w-96 mx-auto text-center">
        <span class="text-2xl font-light ">Login to your account</span>
        <div class="mt-4 bg-white shadow-md rounded-lg text-left">
          <div class="h-2 bg-purple-400 rounded-t-md"></div>
          <div class="px-8 py-6 ">
            <TextField
              label="Username or Email"
              onChange={(v: string) => {
                setCredentials({ ...credentials(), username: v });
              }}
            />
            <TextField
              label="Password"
              mask={true}
              onChange={(v: string) => {
                setCredentials({ ...credentials(), password: v });
              }}
            />
            <div class="flex justify-between items-baseline">
              <button
                type="submit"
                class="mt-4 bg-purple-500 text-white py-2 px-6 rounded-md hover:bg-purple-600"
                onClick={onSubmit}
              >
                Login
              </button>
              {/* <a href="#" class="text-sm hover:underline">
                Forgot password?
              </a> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
