import { Component } from 'solid-js';

interface Props {
  label: string;
  onClick: () => Promise<void> | void;
}

const Button: Component<Props> = props => {
  return (
    <button
      type="button"
      class="mt-4 bg-purple-500 text-white py-2 px-6 rounded-md hover:bg-purple-600"
      onClick={props.onClick}
    >
      {props.label}
    </button>
  );
};

export default Button;
