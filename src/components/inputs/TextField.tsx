import { FaSolidEye, FaSolidEyeSlash } from 'solid-icons/fa';
import { Component, Show, createMemo, createSignal } from 'solid-js';

interface Props {
  label?: string;
  mask?: boolean;
  value?: string | null;
  xxx?: string | null;
  placeholder?: string;
  onChange?: (value: string) => void;
}

const TextField: Component<Props> = props => {
  const [showPassword, setShowPassword] = createSignal(false);

  const toggleShowPassword = () => setShowPassword(!showPassword());

  const inputType = createMemo(() => {
    return props.mask ? (showPassword() ? 'text' : 'password') : 'text';
  });

  return (
    <div>
      {props.label && <label class="block mt-3 font-semibold">{props.label}</label>}
      <div class="relative">
        <input
          value={props.value ?? null}
          type={inputType()}
          placeholder={props.placeholder}
          class="border w-full h-5 px-3 py-5 mt-2 hover:outline-none focus:outline-none focus:ring-indigo-500 focus:ring-1 rounded-md"
          onChange={e => {
            if (props.onChange) {
              props.onChange(e.target.value);
            }
          }}
        />
        {props.mask && (
          <span class="absolute top-5 right-2 cursor-pointer">
            <Show
              when={showPassword()}
              fallback={
                <span onClick={toggleShowPassword}>
                  <FaSolidEye />
                </span>
              }
            >
              <span onClick={toggleShowPassword}>
                <FaSolidEyeSlash />
              </span>
            </Show>
          </span>
        )}
      </div>
    </div>
  );
};

export default TextField;
