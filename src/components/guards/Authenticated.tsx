import { Outlet, useNavigate } from '@solidjs/router';
import { useStore } from '@store';
import { Component, createEffect } from 'solid-js';
import { StorageKey } from '../../helper/constants';

const Authenticated: Component<{}> = props => {
  const [, { setProfile }] = useStore();
  const navigate = useNavigate();
  const token = sessionStorage.getItem(StorageKey.ACCESS_TOKEN);

  createEffect(async () => {
    if (token) {
      await setProfile();
      navigate('/', { replace: true });
    }
  });

  return <Outlet />;
};

export default Authenticated;
