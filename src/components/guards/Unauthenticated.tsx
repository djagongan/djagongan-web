import { Outlet, useNavigate } from '@solidjs/router';
import { useStore } from '@store';
import { io } from 'socket.io-client';
import { Component, createEffect } from 'solid-js';
import { StorageKey } from '../../helper/constants';

const Unauthenticated: Component<{}> = props => {
  const [, { setProfile, setSocket }] = useStore();
  const navigate = useNavigate();
  const token = sessionStorage.getItem(StorageKey.ACCESS_TOKEN);

  createEffect(async () => {
    if (!token) {
      navigate('/signin', { replace: true });
    } else {
      await setProfile();
      const socket = io(import.meta.env.VITE_SOCKET_URL, {
        auth: {
          token: sessionStorage.getItem(StorageKey.ACCESS_TOKEN)
        }
      });
      setSocket(socket);
    }
  });

  return <Outlet />;
};

export default Unauthenticated;
