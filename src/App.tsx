import { useRoutes } from '@solidjs/router';
import { Component } from 'solid-js';
import routes from './routes';

const App: Component = () => {
  const Routes = useRoutes(routes);
  return <Routes />;
};

export default App;
