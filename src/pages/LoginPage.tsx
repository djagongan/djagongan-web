import type { Component } from 'solid-js';
import Login from '../components/forms/Login';

const LoginPage: Component = () => {
  return <Login />;
};

export default LoginPage;
