import type { Component } from 'solid-js';

const AboutPage: Component = () => {
  return <p class="text-4xl text-red-400-700 text-center py-20">AboutPage!</p>;
};

export default AboutPage;
