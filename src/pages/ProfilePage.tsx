// import TextField from '@components/inputs/TextField';
import { Button, TextField } from '@components';
import { ISocketUserEvent, IUser } from '@models';
import { useStore } from '@store';
import { createEffect, createMemo, createSignal, on, onCleanup, onMount, type Component } from 'solid-js';

const ProfilePage: Component = () => {
  const [state, { setProfile }] = useStore();

  const [data, setData] = createSignal<IUser>({
    firstName: '',
    lastName: ''
  } as IUser);

  const eventNames = createMemo(() => ({
    getProfile: 'getProfile:' + state().profile._id
  }));

  const onGetProfile = (data: IUser) => {
    setProfile();
  };

  onMount(() => {
    setTimeout(() => {
      state().socket.on(eventNames().getProfile, onGetProfile);
    }, 1000);
  });

  onCleanup(() => {
    state().socket.off(eventNames().getProfile, onGetProfile);
  });

  createEffect(
    on(state, _state => {
      setData(_state.profile);
    })
  );

  return (
    <div>
      <div>
        <div>FirstName: {state().profile.firstName}</div>
        <div>LastName: {state().profile.lastName}</div>
        <div>Email: {state().profile.email}</div>
      </div>
      <div class="mt-4">
        <TextField value={data().firstName} label="First Name" onChange={v => setData({ ...data(), firstName: v })} />
        <TextField value={data().lastName} label="Last Name" onChange={v => setData({ ...data(), lastName: v })} />
        <Button
          label="Update"
          onClick={() => {
            const payload: ISocketUserEvent = {
              action: 'updateProfile',
              data: data()
            };
            state().socket.emit('user', payload);
          }}
        />
      </div>
    </div>
  );
};

export default ProfilePage;
