import { IUser } from '@models';
import { authService } from '@services';
import { Socket } from 'socket.io-client';
import { Accessor, JSX, createContext, createSignal, useContext } from 'solid-js';

export interface IStoreState {
  profile: IUser;
  socket: Socket;
}

interface IStoreAction {
  setProfile: () => Promise<void>;
  setSocket: (socket: Socket) => void;
}

type IStoreContext = [Accessor<IStoreState>, IStoreAction];

const StoreContext = createContext<IStoreContext>();

interface Props {
  initial: IStoreState;
  children: JSX.Element;
}

export function StoreProvider(props: Props) {
  const [state, setState] = createSignal<IStoreState>(props.initial || ({} as IStoreState));

  const data: IStoreContext = [
    state,
    {
      async setProfile() {
        const res = await authService.profile();
        if (res.data) {
          setState({ ...state(), profile: res.data });
        }
      },
      setSocket(socket) {
        setState({ ...state(), socket });
        socket.on('connect', () => {
          console.log('Connection Id', socket.id, state().profile._id);
        });
      }
    }
  ];

  return <StoreContext.Provider value={data}>{props.children}</StoreContext.Provider>;
}

export function useStore() {
  return useContext(StoreContext);
}
