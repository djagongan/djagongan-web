import { StorageKey, http } from "@helper";
import { IPayloadLogin, IResponse, IResponseLogin, IUser } from "@models";

export default {
  async login(payload: IPayloadLogin): Promise<boolean> {
    try {
      const res = await http.post<IResponse>('/auth/login', payload)
      const data: IResponseLogin = res.data.data
      sessionStorage.setItem(StorageKey.ACCESS_TOKEN, data.accessToken)
      sessionStorage.setItem(StorageKey.REFRESH_TOKEN, data.refreshToken)
      return true
    } catch (error) {
      return false
    }
  },
  async profile(): Promise<{ data?: IUser, error?: string }> {
    try {
      const res = await http.get<IResponse>('/auth/profile')
      return {
        data: res.data.data,
      }
    } catch (error) {
      return {
        error
      }
    }
  }
}