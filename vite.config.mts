import path from 'path';
import devtools from 'solid-devtools/vite';
import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';

export default defineConfig({
  plugins: [
    /* 
    Uncomment the following line to enable solid-devtools.
    For more info see https://github.com/thetarnav/solid-devtools/tree/main/packages/extension#readme
    */
    devtools({
      autoname: true,
      locator: true
    }),
    solidPlugin(),
  ],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
  },
  resolve: {
    alias: {
      "@src": path.resolve(__dirname, './src'),
      "@helper": path.resolve(__dirname, './src/helper/index.ts'),
      "@components": path.resolve(__dirname, './src/components/index.tsx'),
      "@pages": path.resolve(__dirname, './src/pages'),
      "@services": path.resolve(__dirname, './src/services/index.ts'),
      "@models": path.resolve(__dirname, './src/models/index.ts'),
      "@store": path.resolve(__dirname, './src/store/index.tsx')
    }
  }
});
